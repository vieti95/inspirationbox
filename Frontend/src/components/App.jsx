import {
  FluentProvider,
  teamsLightTheme,
  teamsDarkTheme,
  teamsHighContrastTheme,
  tokens,
} from "@fluentui/react-components";
import { useEffect } from "react";
import { HashRouter as Router, Navigate, Route, Routes } from "react-router-dom";
import { app } from "@microsoft/teams-js";
import { useTeamsUserCredential } from "@microsoft/teamsfx-react";
import Privacy from "./Privacy";
import TermsOfUse from "./TermsOfUse";
import Tab from "./Tab";
import TabConfig from "./TabConfig";
import { TeamsFxContext } from "./Context";
import config from "./sample/lib/config";
import {Get} from "./sample/Get";
import {Post} from "./sample/Post";
import {Homepage} from "./sample/Homepage/Homepage";
import {ProjectAdd} from "./sample/ProjectBox/ProjectAdd";
import {ProjectBox} from "./sample/ProjectBox/ProjectBox";
import {ProjectEdit} from "./sample/ProjectBox/ProjectEdit";
import {ProjectSearchList} from "./sample/ProjectBox/ProjectSearchList";
import {ProjectView} from "./sample/ProjectBox/ProjectView";
import { ProjectSearchSector } from "./sample/ProjectBox/ProjectSearchSector";
import {Banking} from "./Sector/Banking";
import {Defence} from "./Sector/Defence";
import {Energy} from "./Sector/Energy";
import {Health} from "./Sector/Health";
import {Insurance} from "./Sector/Insurance";
import {Life} from "./Sector/Life";
import {Manufacturing} from "./Sector/Manufacturing";
import {Media} from "./Sector/Media";
import {Public} from "./Sector/Public";
import {Retail} from "./Sector/Retail";
import {Space} from "./Sector/Space";
import {Telecommunication} from "./Sector/Telecommunication";
import {Transport} from "./Sector/Transport";
import{InitTeamsFx} from "../sso/InitTeamsFx";
import { IdeaBox } from "./sample/IdeaBox/IdeaBox";
import { IdeaAdd } from "./sample/IdeaBox/IdeaAdd";
import { IdeaSearchInnovative} from "./sample/IdeaBox/IdeaSearchInnovative";
import { IdeaEdit } from "./sample/IdeaBox/IdeaEdit";
import { IdeaView } from "./sample/IdeaBox/IdeaView";
import { Technology } from "./sample/Innovative/Technology";
import { ProjectManager } from "./sample/Innovative/ProjectManager";
import { Newindustry } from "./sample/Innovative/NewIndustry";
import { Focustopic } from "./sample/Innovative/Focustopic";
import {InspirationProject} from "./sample/InspirationBox/InspirationProject";
import { Connectivity } from "./sample/Megatrend/Connectivity";
import { GenderShift } from "./sample/Megatrend/Gender-Shift";
import { Globalisation } from "./sample/Megatrend/Globalisation";
import { Healthtrend } from "./sample/Megatrend/Health";
import { Individualisation } from "./sample/Megatrend/Individualisation";
import { KnowledgeCulture } from "./sample/Megatrend/Knowledge-Culture";
import { Mobility } from "./sample/Megatrend/Mobility";
import { NeoEcology } from "./sample/Megatrend/Neo-Ecology";
import { NewWork } from "./sample/Megatrend/New-Work";
import { Security } from "./sample/Megatrend/Security";
import { SilverAge } from "./sample/Megatrend/Silver-Age";
import { Urbanisation } from "./sample/Megatrend/Urbanisation";
import { PersonalUser } from "./sample/Personal/Personal-User";
import { PersonalIdea } from "./sample/Personal/Personal-Idea";
import { Personalproject } from "./sample/Personal/Personal-Project";
import {FAQ} from "./sample/FAQ/faq";

export default function App() {
  const { loading, theme, themeString, teamsUserCredential } = useTeamsUserCredential({
    initiateLoginEndpoint: config.initiateLoginEndpoint,
    clientId: config.clientId,
  });

  useEffect(() => {
    loading &&
      app.initialize().then(() => {
        // Hide the loading indicator.
        app.notifySuccess();
      });
  }, [loading]);
  return (
    <TeamsFxContext.Provider value={{ theme, themeString, teamsUserCredential }}>
      <FluentProvider
        theme={
          themeString === "dark"
            ? teamsDarkTheme
            : themeString === "contrast"
            ? teamsHighContrastTheme
            : {
                ...teamsLightTheme,
                colorNeutralBackground3: "#eeeeee",
              }
        }
        style={{ background: tokens.colorNeutralBackground3 }}
      >
        <Router>
          {!loading && (
            <Routes>
              <Route path="/privacy" element={<Privacy />} />
              <Route path="/projectadd" element={<ProjectAdd/>} />
              <Route path="/termsofuse" element={<TermsOfUse />} />
              <Route path="/tab" element={<Tab />} />
              <Route path="/config" element={<TabConfig />} />
              <Route path="/get" element={  <Get />} />
              <Route path="/post" element={  <Post />} />
              <Route path="/homepage" element={  <Homepage />} />
              <Route path="/" element={<Navigate to={"/homepage"} />}></Route>
              <Route path="/projectbox" element={<ProjectBox />} />
              <Route path="/projectSearchList" element={<ProjectSearchList/>}/>
              <Route path='/projectview/:id' element={<ProjectView/>}/>
              <Route path='/projectedit/:id' element={<ProjectEdit/>}/>
              <Route path="/projectSearchSector" element={<ProjectSearchSector/>}/>
              <Route path="/banking" element={<Banking/>}/>
              <Route path="/defence" element={<Defence/>}/>
              <Route path="/energy" element={<Energy/>}/>
              <Route path="/health" element={<Health/>}/>
              <Route path="/insurance" element={<Insurance/>}/>
              <Route path="/life" element={<Life/>}/>
              <Route path="/manufacturing" element={<Manufacturing/>}/>
              <Route path="/media" element={<Media/>}/>
              <Route path="/public" element={<Public/>}/>
              <Route path="/retail" element={<Retail/>}/>
              <Route path="/space" element={<Space/>}/>
              <Route path="/telecommunication" element={<Telecommunication/>}/>
              <Route path="/transport" element={<Transport/>}/>
              <Route path="/initTeamsFx" element={<InitTeamsFx/>}/>
              <Route path="/ideabox" element={<IdeaBox/>}/>
              <Route path="/ideaadd" element={<IdeaAdd/>}/>
              <Route path="/ideaSearchInnovative" element={<IdeaSearchInnovative/>}/>
              <Route path="/ideaedit/:id" element={<IdeaEdit/>}/>
              <Route path="/ideaview/:id" element={<IdeaView/>}/>
              <Route path="/technology" element={<Technology/>}/>
              <Route path="/projectmanager" element={<ProjectManager/>}/>
              <Route path="/newindustry" element={<Newindustry/>}/>
              <Route path="/focustopic" element={<Focustopic/>}/>
              <Route path="/inspiration" element={<InspirationProject/>}/>
              <Route path="/connectivity" element={<Connectivity/>}/>
              <Route path="/gendershift" element={<GenderShift/>}/>
              <Route path="/globalisation" element={<Globalisation/>}/>
              <Route path="/healthtrend" element={<Healthtrend/>}/>
              <Route path="/individualisation" element={<Individualisation/>}/>
              <Route path="/knowledgeculture" element={<KnowledgeCulture/>}/>
              <Route path="/mobility" element={<Mobility/>}/>
              <Route path="/neoecology" element={<NeoEcology/>}/>
              <Route path="/newwork" element={<NewWork/>}/>
              <Route path="/security" element={<Security/>}/>
              <Route path="/silverage" element={<SilverAge/>}/>
              <Route path="/urbanisation" element={<Urbanisation/>}/>
              <Route path="/personalUser" element={<PersonalUser/>}/>
              <Route path="/personalIdea" element={<PersonalIdea/>}/>
              <Route path="/personalproject" element={<Personalproject/>}/>
              <Route path="/faq" element={<FAQ/>}/>
            </Routes>
          )}
        </Router>
      </FluentProvider>
    </TeamsFxContext.Provider>
  );
}
