import "./Media.css";
import React from "react";
import { useState,useEffect } from "react";
import axios from "axios";
import { Text } from "@fluentui/react-components";
import {  useNavigate } from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";

export function Media(props) {
  const [iGet, setGet] = useState([]);
 const navi = useNavigate();
 const Sectors = new Map();
  const Aspect= new Map();

  Sectors.set(7,"Media")

  Aspect.set(0,"Technology")
  Aspect.set(1,"Project Managment")
  Aspect.set(2,"New industry")
  Aspect.set(3,"Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])")

  useEffect(() => {
    axios
        .get("http://localhost:5248/api/Project/")
        .then((response) => {
          setGet(response.data);
            console.log(response);
        })
        .catch((err) => console.log(err));
}, []);

const Edit=(id)=>{
  navi('/projectedit/'+id)
}

  const renderTable = () => {
    return iGet.map(user => {
     
        if(user.sector===7)
        {
          return(
          
            <tr>
            <td>{user.title}</td>
            <td>{Sectors.get(user.sector)}</td>
            <td>{Aspect.get(user.aspect)}</td> 
            <td>{user.description}</td>
            <td>{user.contactPerson}</td>
            <td><button className="View" onClick={()=>navi('/projectview')}>View
            </button>
            /
            <button className="Edit" onClick={()=>{Edit(user.idnr)}}>Edit
            </button></td>
            </tr>
                  )
        }
      
      })
  }



  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/projectSearchSector')}/>
      <div className="Media_Title">
      <Text className="Text">Media</Text>
      </div>
     <table className="Sector_table" >
          <thead className="thead">
          <tr className="tr">
          <th className="th1">Title</th>
          <th className="th2">Sector & Industry</th>
          <th className="th3">Innovative Aspect</th>
          <th className="th4">Description</th>
          <th className="th5">ContactPerson</th>
          <th className="th6">View/Edit</th>
         </tr>
         </thead>
         <tbody className="tbody">
         {renderTable()}
          </tbody>
         </table>
    </div>
  );
}
