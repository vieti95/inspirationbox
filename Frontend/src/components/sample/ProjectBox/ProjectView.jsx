import React from "react";
import "./ProjectView.css";
import { Text } from "@fluentui/react-components";
import { useState, useEffect} from "react";
import { useNavigate, useParams} from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";
import axios from "axios";
export function ProjectView(props)
{
  const [iData, setData] = useState([]);
  const { title, sector, aspect, description, contactPerson } = iData;
  const navi = useNavigate();
  const { id } = useParams();
  useEffect(() => {
    loadUser();

  }, []);
  const loadUser = async() => {
    const result =await axios.get(`http://localhost:5248/api/Project/${id}`);
    setData(result.data);

  };

 
  return(
    <div>
      <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/projectSearchList')}/>
      <div className="View_Title">
      <Text className="Text">{title}</Text>
      </div>  
      
      
    </div>
      
    </div>
  );
}