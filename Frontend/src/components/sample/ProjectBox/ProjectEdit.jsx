import axios from "axios";
import "./ProjectEdit.css";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams} from "react-router-dom";
import { Text } from "@fluentui/react-components";
import {useRef} from 'react';
import { LiaArrowLeftSolid} from "react-icons/lia";
import ReactModal from 'react-modal';

export function ProjectEdit(props) {
  let navigate = useNavigate();
  
  const titleRef = useRef(null);
    const aspectRef = useRef(null);
    const sectorRef=useRef(null);
    const descriptionRef = useRef(null);
  const { id } = useParams();
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useState({});
  

  const { title, sector, aspect, description, contactPerson } = user;
 
  const EditDelet =()=>
  {
    axios.delete(`http://localhost:5248/api/Project/${id}`)
   .then((response)=>{
    console.log(response)
    console.log("delete")
    navigate(-1)
   })
   
  }
  
  const Update=()=>{
    setIsOpen(false)
    navigate(-1)
  }

  const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();

  }, []);

    const onSubmit = (e) => {
    console.log(titleRef.current.value);
    console.log(aspectRef.current.value);
    console.log(sectorRef.current.value);
    console.log(descriptionRef.current.value);
    var intaspect= parseInt(aspectRef.current.value);
    var intsector= parseInt(sectorRef.current.value);
    e.preventDefault();
    axios.put(`http://localhost:5248/api/Project/${id}`,{title: titleRef.current.value,
    aspect: intaspect,
    creatoridnr: 1,
    description: descriptionRef.current.value,
    contactpersonidnr: 1,
    sector: intsector,
  })
    .then((res)=>{
      console.log(res)
      console.log("erfolgreich")
      setIsOpen(true)  
     })
     .catch(function (error) {
      console.log(error);
     
    })
  };

  const loadUser = async() => {
    const result =await axios.get(`http://localhost:5248/api/Project/${id}`);
    setUser(result.data);

  };


  return (
      <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navigate('/projectSearchList')}/>
      <div className="Edit_Title">
      <Text className="Text">{title}</Text>
      </div>
     

          <form onSubmit={(e) => onSubmit(e)}>
            <div className="EditAddtitle">
              <label className="Text">
                Project Title
              </label>
              <input
                type={"text"}
                className="Field"
                name="title"
                value={title}
                ref={titleRef}
                onChange={(e)=>onInputChange(e)}
              />
            </div>
            <div className="EditSector">
            <label className="Text">
                Sector & Industry
              </label>
              
              
            <select
           
                            name="sector"
                            value={sector}
                            onChange={(e) => onInputChange(e)}
                            ref={sectorRef}
                            className="select"
                            aria-label="Filter Countries By Sector & Industry"
                        >
                            <option value="" disabled selected>Sector & Industry</option>
      <option type="number" value="0">Banking</option>
      <option type="number" value="1">Defence & Intelligence</option>
      <option type="number" value="2">Energy & Utilities</option>
      <option type="number" value="3">Health</option>
      <option type="number" value="4">Insurance</option>
      <option type="number" value="5">Life Sciences</option>
      <option type="number" value="6">Manufacturing</option>
      <option type="number" value="7">Media</option>
      <option type="number" value="8">Public</option>
      <option type="number" value="9">Retail & Consumer Services</option>
      <option type="number" value="10">Space</option>
      <option type="number" value="11">Telecommunication</option>
      <option type="number" value="12">Transportation & Logistics</option>
                        </select>
                        </div>
                        <div className="EditInnovative">
                        <label className="Text">
                         Innovative aspect
                        </label>
                        <select
                            name="aspect"
                            value={aspect}
                            ref={aspectRef}
                            onChange={(e) => onInputChange(e)}
                            className="select"
                        >
                          <option value="" disabled selected>Innovative aspect</option>
      <option type="number" value="0">Technology</option>
      <option type="number" value="1">Project Management</option>
      <option type="number" value="2">New industry</option>
      <option type="number" value="3">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
                        </select>
                </div>

                <div className="EditDescription">
                      <label className="Text">
                       Short Description
                       </label>
                       <input
                       type={"text"}
                className="Field"
                name="description"
                value={description}
                ref={descriptionRef}
                onChange={(e) => onInputChange(e)}
              />
            </div>

            <div className="EditContact">
              <label  className="Text">
                Contact Person
              </label>
              <input
                type={"text"}
                className="Field"
                placeholder="Enter your e-mail address"
                name="contactPerson"
                value={contactPerson}
                onChange={(e) => onInputChange(e)}
              />
            </div>
            
            <div className="EditSubmit">
            <button  type="submit" className="SubmitButton">
              Submit
            </button>
            {isOpen&& (
                 
                 <ReactModal className="Popup"
                 isOpen={isOpen}
               >
                 <div className="Edit_Success">
                 <p className="Titeltext">{titleRef.current.value}</p> 
                <p className="Text">Success Submitted</p>
                 
                 <button className="Button" onClick={()=>Update()}>Close</button>
                 </div>
               </ReactModal>    
               )}
            </div>   
            </form>
            <div className="EditDelete">
            <button className="DeleteButton" onClick={()=>EditDelet()} >
              Delete
            </button>
            </div> 
        </div>
    
  );
}