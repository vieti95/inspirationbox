import "./ProjectSearchList.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi';
import { useState, useEffect} from "react";
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import {  useNavigate } from "react-router-dom";
import { FaLightbulb } from 'react-icons/fa'
import { LiaArrowLeftSolid} from "react-icons/lia";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

export function ProjectSearchList(props)
{
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [q, setQ] = useState("");
  const [filterParamsector, setFilterParamsector] = useState(["All"]);
  const [filterParamaspect, setFilterParamaspect] = useState(["All"]);
  const navi =useNavigate();
  const Sectors = new Map();
  const Aspect= new Map();

  Sectors.set(0, "Banking");
  Sectors.set(1,"Defence & Intelligence")
  Sectors.set(2,"Energy & Utilities")
  Sectors.set(3,"Health")
  Sectors.set(4,"Insurance")
  Sectors.set(5,"Life Sciences")
  Sectors.set(6,"Manufacturing")
  Sectors.set(7,"Media")
  Sectors.set(8,"Public")
  Sectors.set(9,"Retail & Consumer Services")
  Sectors.set(10,"Space")
  Sectors.set(11,"Telecommunication")
  Sectors.set(12,"Transportation & Logistics")

  Aspect.set(0,"Technology")
  Aspect.set(1,"Project Management")
  Aspect.set(2,"New industry")
  Aspect.set(3,"Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector]")
  
  useEffect(() => {
  fetch("http://localhost:5248/api/Project")
  .then((res) => res.json())
  .then(
  (result) => 
  {
    setIsLoaded(true);
    setItems(result);
  },
  (error) => 
  {
    setIsLoaded(true);
    setError(error);
    
  }
  );
  }, []);

  const Edit=(id)=>{
    navi('/projectedit/'+id)
  }

  const View=(id)=>{
    navi('/projectview/'+id)
  }
    function search(items) {
      return items.filter((item) => {
            if (Sectors.get(item.sector) === filterParamsector && Aspect.get(item.aspect)===filterParamaspect ) {
             
                return item.title.toLowerCase().includes(
                    q.toLowerCase())
                    ||
                    Sectors.get(item.sector).toLowerCase().includes(
                        q.toLowerCase())
                    ||
                   Aspect.get(item.aspect).toLowerCase().includes(
                        q.toLowerCase())
                    || item.description.toLowerCase().includes(q.toLowerCase())

                   } else if (filterParamsector === "All" && Aspect.get(item.aspect)===filterParamaspect) {
                return item.title.toLowerCase().includes(
                    q.toLowerCase())
                    ||
                    Sectors.get(item.sector).toLowerCase().includes(
                        q.toLowerCase())
                    ||
                   Aspect.get(item.aspect).toLowerCase().includes(
                        q.toLowerCase())
                    || item.description.toLowerCase().includes(q.toLowerCase())
            }
            else if (filterParamaspect === "All" && Sectors.get(item.sector) === filterParamsector) {
                return item.title.toLowerCase().includes(
                    q.toLowerCase())
                    ||
                    Sectors.get(item.sector).toLowerCase().includes(
                        q.toLowerCase())
                    ||
                   Aspect.get(item.aspect).toLowerCase().includes(
                        q.toLowerCase())
                    || item.description.toLowerCase().includes(q.toLowerCase())
            }
            else if(filterParamsector==='All'&&filterParamaspect==='All')
    {
        return item.title.toLowerCase().includes(
            q.toLowerCase())
            ||
            Sectors.get(item.sector).toLowerCase().includes(
                q.toLowerCase())
            ||
           Aspect.get(item.aspect).toLowerCase().includes(
                q.toLowerCase())
            || item.description.toLowerCase().includes(q.toLowerCase())
    }
        });
    }
    
    if (error) {
        return (
            <p>
                {error.message}
            </p>
        );
    } else if (!isLoaded) {
        return <>loading...</>;
    } else {
        return (
            <div>
              <div className="Border">
              <div className="Project_Titel">
              <Text className="Project_SearchListText"> <GiBrain className="Project_icon"> </GiBrain> Search by list</Text>
              <LiaArrowLeftSolid className="PfeilbackProjectSearchList" onClick={()=>navi('/projectbox')}/>
              </div>
      
              <div className="Project_Menue">
              <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
              <p ><Link className="Project" to="/projectbox"><GiBrain className="Projecticon"> </GiBrain></Link></p>
              <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Project_Ideaicon"> </FaLightbulb></Link></p>
              <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Project_Inspirationicon"> </BsRainbow></Link></p>
              <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Project_PersonalUsericon"> </PersonIcon></Link></p>
              <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Project_faqicon"> </QuestionAnswerIcon></Link></p>
              </div>
                <div>
                    <label>
                        <input type="text"
                            name="search-form"
                            id="search-form"
                            className="search-input"
                            placeholder="Search for..."
                           
                            onChange={(e)=>setQ(e.target.value)}
                        />
                    
                        <select
                            onChange={(e) => {
                                setFilterParamsector(e.target.value);
                            }}
                            className="sectorselect"
                            aria-label="Filter Countries By Sector & Industry"
                        >
                           <option value="" disabled selected>Sector & Industry</option>
                           <option value="All">All Sector & Industry</option>
                           <option value="Banking">Banking</option>
                           <option value="Defence & Intelligence">Defence & Intelligence</option>
                           <option value="Energy & Utilities">Energy & Utilities</option>
                           <option value="Health">Health</option>
                           <option value="Insurance">Insurance</option>
                           <option value="Life Sciences">Life Sciences</option>
                           <option value="Manufacturing">Manufacturing</option>
                           <option value="Media">Media</option>
                           <option value="Public">Public</option>
                           <option value="Retail & Consumer Services">Retail & Consumer Services</option>
                           <option value="Space">Space</option>
                           <option value="Telecommunication">Telecommunication</option>
                           <option value="Transportation & Logistics">Transportation & Logistics</option>
                           </select>

                        <select
                            onChange={(e) => {
                                setFilterParamaspect(e.target.value);
                            }}
                            className="innovativeselect"
                            aria-label="Filter Countries By Innovative Aspect"
                        >
                           <option value="" disabled selected>Innovative Aspect</option>
                           <option value="All">ALL Innovative Aspect</option>
                           <option value="Technology">Technology</option>
                           <option value="Project Management">Project Management</option>
                           <option value="New industry">New industry</option>
                          <option value="Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector]">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
                        </select>
                        </label>
                </div>

                <table className="table" >
                <thead className="thead">
                <tr className="tr">
                <th className="th1">Title</th>
                <th className="th2">Sector & Industry</th>
                <th className="th3">Innovative Aspect</th>
                <th className="th4">Description</th>
                <th className="th5">ContactPerson</th>
                <th className="th6">View/Edit</th>
                </tr>
                </thead>
        
                
                   { search(items).map((item) => (
                        <tbody className="tbody"> 
                        <td>{item.title}</td>
                        <td>{Sectors.get(item.sector)}</td>
                        <td>{Aspect.get(item.aspect)}</td>
                        <td>{item.description}</td>
                        <td>{item.contactPerson}</td>
                        <td><button className="View" onClick={()=>View(item.idnr)}>View
                        </button>
                        /
                        <button className="Edit" onClick={()=>{Edit(item.idnr)}}>Edit
                        </button></td>
                        </tbody>
                    ))}
                   
                </table>
            </div>
            </div>
        );
    }
}
  
