import "./ProjectAdd.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi'
import { IoIosAddCircle } from "react-icons/io";
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import axios from "axios";
import {useRef} from 'react';
import { useState} from "react";
import ReactModal from 'react-modal';
import { FaLightbulb } from 'react-icons/fa'
import {  useNavigate } from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';


const baseURL = "http://localhost:5248/api/Project";

export function ProjectAdd(props)
{
    const navi =useNavigate();
    const [message, setMessage] = useState(null);
    const Project_titleRef = useRef(null);
    const Project_aspectRef = useRef(null);
    const Project_sectorRef=useRef(null);
    const Project_descriptionRef = useRef(null);
    //const contactRef = useRef(null);
    const [isOpen, setIsOpen] = useState(false);
    
    const Close=()=>{
      setIsOpen(false)
      navi(-1)
    }

  return(
    <div>
      <div className="Border">
       <div className="Project_Titel">
      <Text className="Project_AddText"> <GiBrain className="Project_icon"> </GiBrain> Add a project</Text>
      <LiaArrowLeftSolid className="PfeilbackProjectAdd" onClick={()=>navi('/projectbox')}/>
      </div>
      <div className="Project_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Project_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Project_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Project_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Project_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
      <div className="Add">
      <IoIosAddCircle className="icon"></IoIosAddCircle>
      <div className="Addtitle">
      <input ref={Project_titleRef} type="text" className="Text" placeholder="Project title" size="52"></input>
      </div>
      <div className="Sector">
      <select  ref={Project_sectorRef} className="select" name="Branchen" placeholder="Sector & Industry">
      <option value="" disabled selected>Sector & Industry</option>
      <option type="number" value="0">Banking</option>
      <option type="number" value="1">Defence & Intelligence</option>
      <option type="number" value="2">Energy & Utilities</option>
      <option type="number" value="3">Health</option>
      <option type="number" value="4">Insurance</option>
      <option type="number" value="5">Life Sciences</option>
      <option type="number" value="6">Manufacturing</option>
      <option type="number" value="7">Media</option>
      <option type="number" value="8">Public</option>
      <option type="number" value="9">Retail & Consumer Services</option>
      <option type="number" value="10">Space</option>
      <option type="number" value="11">Telecommunication</option>
      <option type="number" value="12">Transportation & Logistics</option>
      </select>
      </div>
      <div className="Innovative">
      <select ref={Project_aspectRef} className="select" name="Innovative" placeholder="Innovative aspect">
      <option value="" disabled selected>Innovative aspect</option>
      <option type="number" value="0">Technology</option>
      <option type="number" value="1">Project Management</option>
      <option type="number" value="2">New industry</option>
      <option type="number" value="3">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
      </select>
      </div>
      <div className="Description">
      <input ref={Project_descriptionRef} type="text" className="Text" placeholder="Short description" size="52"></input>
      </div>
      <div className="Addtitle">
      <div className="Project_Submitt">
                <button className="AddButton" onClick={() => {
                  console.log(Project_titleRef.current.value);
                  console.log(Project_aspectRef.current.value);
                  console.log(Project_sectorRef.current.value);
                  console.log(Project_descriptionRef.current.value);
                  var intaspect= parseInt(Project_aspectRef.current.value);
                  var intsector= parseInt(Project_sectorRef.current.value);
                  axios.post(baseURL, {
                    title: Project_titleRef.current.value,
                    aspect: intaspect,
                    sector: intsector,
                    creatoridnr: 1,
                    description: Project_descriptionRef.current.value,
                    contactpersonidnr: 1
                  })
                    .then(function (response) {
                      console.log(response);
                      //setPost(response.data);
                      setMessage({type: 'success', response});
                      setIsOpen(true)
                      
                    })
                    .catch(function (error) {
                      console.log(error);
                      setMessage({type:'error', error});
                    });
      
              }} >Submit</button> 
              {message?.type==='error'&&(alert("Please fill in all fields "))}
              {isOpen&& (
                 
                <ReactModal className="Popup"
                isOpen={isOpen}
              >
                <div className="Project_Success">
                <p className="Titeltext">{Project_titleRef.current.value}</p> 
                <p className="Text">Success Submitted</p>
                
                <button className="Button" onClick={()=>Close()}>Close</button>
                </div>
              </ReactModal>    
              )}
   </div>  
      </div>
      </div>
      </div>
      </div>
 );
}