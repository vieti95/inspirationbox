import "./ProjectSearchSector.css";
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import ShieldIcon from '@mui/icons-material/Shield';
import EnergySavingsLeafIcon from '@mui/icons-material/EnergySavingsLeaf';
import HealingIcon from '@mui/icons-material/Healing';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import { MdScience } from "react-icons/md"; 
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing';
import HeadsetMicIcon from '@mui/icons-material/HeadsetMic';
import SavingsIcon from '@mui/icons-material/Savings';
import { HiShoppingCart} from "react-icons/hi";
import {BsRocketTakeoffFill} from "react-icons/bs";
import CellTowerIcon from '@mui/icons-material/CellTower';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { GiBrain } from 'react-icons/gi';
import { AiFillHome } from "react-icons/ai";
import {  Link } from "react-router-dom";
import { Text } from "@fluentui/react-components";
import {  useNavigate } from "react-router-dom";
import {
  CircleMenu,
  CircleMenuItem,
 
} from "react-circular-menu";
import { LiaArrowLeftSolid} from "react-icons/lia";
import { FaLightbulb } from 'react-icons/fa'
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';

export function ProjectSearchSector(props){
  const navi =useNavigate();

  return (
      <div className="Border">
        <div className="Project_Titel">
              <Text className="Project_SearchSectorText"> <GiBrain className="Project_icon"> </GiBrain> Search by Sector</Text>
              <LiaArrowLeftSolid className="PfeilbackProjectSearchSector" onClick={()=>navi('/projectbox')}/>
              </div>
      <div className="Project_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Project_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Project_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Project_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Project_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "90vh",
        }}
      >
        
        <CircleMenu
          startAngle={-90}
          rotationAngle={360}
          itemSize={3}
          radius={12}
         
          rotationAngleInclusive={false}
          className={"asean-symbol"}
        >
          
          <CircleMenuItem
          tooltip="Banking"  
          onClick={()=>navi('/banking')}
          >
          
              <SavingsIcon className="SavingsIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Defence & Intelligence"
            onClick={()=>navi('/defence')}
          >
              <ShieldIcon className="ShieldIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Engery & Utilities"
            onClick={()=>navi('/energy')}
          >
              <EnergySavingsLeafIcon className="EnergySavingsLeafIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Health"
            onClick={()=>navi('/health')}
          >
              <HealingIcon className="HealingIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Insurance"
            onClick={() => navi('/insurance')}
          >
              <VerifiedUserIcon className="VerifiedUserIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Life Sciences"
            onClick={() => navi('/life')}
          >
              <MdScience className="MdScience"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Manufacturing"
            onClick={() => navi('/manufacturing')}
          >
              <PrecisionManufacturingIcon className="PrecisionManufacturingIcon"/>
          </CircleMenuItem>
          
          <CircleMenuItem
          tooltip="Media"
            onClick={() => navi('/media')}
          >
              <HeadsetMicIcon className="HeadsetMicIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Public"
            onClick={() => navi('/public')}
          >
              <AccountBalanceIcon className="AccountBalanceIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Retail & Consumer Services"
            onClick={() => navi('/retail')}
          >
              <HiShoppingCart className="HiShoppingCart"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Space"
            onClick={() => navi('/space')}
          >
              <BsRocketTakeoffFill className="BsRocketTakeoffFill"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Telecommunication"
            onClick={() => navi('/telecommunication')}
          >
              <CellTowerIcon className="CellTowerIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Transportation & Logistics"
            onClick={() => navi('/transport')}
          >
              <LocalShippingIcon className="LocalShippingIcon"/>
          </CircleMenuItem>
        </CircleMenu>
        
        
      </div>
      </div>
    
  );
}
      