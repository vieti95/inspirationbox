import "./ProjectBox.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi';
import {  useNavigate } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import {  Link } from "react-router-dom";
import { FaLightbulb } from 'react-icons/fa'
import { LiaArrowLeftSolid} from "react-icons/lia";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

export function ProjectBox(props)
{
  const navi =useNavigate();
  return(
    <div>
      <div className="Border">
      <div className="Project_Title">
      <Text className="Project_Text"> <GiBrain className="Project_icon"> </GiBrain> ProjectBox</Text>
      <LiaArrowLeftSolid className="PfeilbackProjectBox" onClick={()=>navi('/homepage')}/>
      </div>
      <div className="Project_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Project_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Project_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Project_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Project_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
      <div className="Sectors">
      <button className="Button" onClick={()=>navi('/projectSearchSector')}>
      <p className="Text">search projects by Sectors</p>  
      </button>
      </div>
      <div className="List">
      <button className="Button" onClick={()=>navi('/projectSearchList')}>
      <p className="Text">search projects by List </p>
      </button>
      </div>
      <div className="Add">
      <button className="Button" onClick={()=>navi('/projectadd')}>
      <p className="Text">add a project</p> 
      </button>
      </div>
      </div>
      </div>
  );
}