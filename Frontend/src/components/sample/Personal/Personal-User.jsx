import "./Personal-User.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi'
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import { FaLightbulb } from 'react-icons/fa'
import {  useNavigate } from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { AiFillLike } from "react-icons/ai";

export function PersonalUser(props)
{
    const navi =useNavigate();

  return(
    <div>
      <div className="Border">
       <div className="PersonaluserTitel">
      <Text className="Text"> <PersonIcon fontSize="large"> </PersonIcon> Personal User</Text>
      <LiaArrowLeftSolid className="Pfeilbackpersonaluser" onClick={()=>navi('/homepage')}/>
      </div>
      <div className="Personal_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Personal_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Personal_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Personal_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Personal_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
     <div className="Personal_Project">
     <button className="button" onClick={()=>navi('/personalproject')}> <GiBrain className="icon"> </GiBrain> 
     <Text className="text">What are my projects</Text> 
     </button>
     </div>
     <div className="Personal_Idea">
     <button className="button" onClick={()=>navi('/personalIdea')}> <FaLightbulb className="icon"> </FaLightbulb> 
     <Text className="text">What are my ideas</Text> 
     </button>
     </div>
     <div className="Personal_Feedback">
     <button className="button" onClick={()=>navi('/')}> <AiFillLike className="icon"> </AiFillLike> 
     <Text className="text">What ideas I applied for /  liked / commented on</Text> 
     </button>
     </div>
     <div className="Personal_Inspiration">
     <button className="button" onClick={()=>navi('/')}> <BsRainbow className="icon"> </BsRainbow> 
     <Text className="text">What are my inspiration</Text> 
     </button>
     </div>
      </div>
      </div>
 );
}