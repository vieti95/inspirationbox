import "./Personal-Idea.css";
import { Text } from "@fluentui/react-components";
import { useState, useEffect} from "react";
import {  useNavigate } from "react-router-dom";
import { FaLightbulb } from 'react-icons/fa';
import { LiaArrowLeftSolid} from "react-icons/lia";


export function PersonalIdea(props)
{
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [q, setQ] = useState("");
  const [filterParamasector, setFilterParamasector] = useState(["All"]);
  const [filterParamaspect, setFilterParamaspect] = useState(["All"]);
  const [filterParampotentialuser, setFilterParampotentialuser] = useState(["All","Sector"]);
  const navi =useNavigate();
  const [show,setShow]=useState(false);
  const Sectors = new Map();
  const Aspect= new Map();
  const Potentialuser= new Map();


  Sectors.set(0, "Banking");
  Sectors.set(1,"Defence & Intelligence")
  Sectors.set(2,"Energy & Utilities")
  Sectors.set(3,"Health")
  Sectors.set(4,"Insurance")
  Sectors.set(5,"Life Sciences")
  Sectors.set(6,"Manufacturing")
  Sectors.set(7,"Media")
  Sectors.set(8,"Public")
  Sectors.set(9,"Retail & Consumer Services")
  Sectors.set(10,"Space")
  Sectors.set(11,"Telecommunication")
  Sectors.set(12,"Transportation & Logistics")
  Sectors.set(13,"Nothing")

  Aspect.set(0,"Technology")
  Aspect.set(1,"Project Management")
  Aspect.set(2,"New industry")
  Aspect.set(3,"Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector]")

  Potentialuser.set(0,"Internally")
  Potentialuser.set(1,"Customer")
  Potentialuser.set(2,"Sector")

  const state=(e)=>{
   if(e==="Sector"){
    setShow(currentShow => !currentShow)
   } 
  }
  
  useEffect(() => {
  fetch("http://localhost:5248/api/Idea")
  .then((res) => res.json())
  .then(
  (result) => 
  {
    setIsLoaded(true);
    setItems(result);
   
    
  },
  (error) => 
  {
    setIsLoaded(true);
    setError(error);
    
  }
  );
  }, []);

  const Edit=(id)=>{
    navi('/ideaedit/'+id)
  }
  function searchidea(items) {
    return items.filter((item) => {
        if(filterParampotentialuser==="Sector")
        {
            if(Sectors.get(item.sector)===filterParamasector && Aspect.get(item.aspect)===filterParamaspect){
            return item.title.toLowerCase().includes(
                q.toLowerCase())
                ||
                Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                    q.toLowerCase())
                    ||
                Sectors.get(item.sector).toLowerCase().includes(
                    q.toLowerCase())
                ||
               Aspect.get(item.aspect).toLowerCase().includes(
                    q.toLowerCase())
                    ||
                    item.aim.toLowerCase().includes(
                        q.toLowerCase())
                || item.description.toLowerCase().includes(q.toLowerCase())
        }
        if(Sectors.get(item.sector)===filterParamasector && filterParamaspect==='All'){
            return item.title.toLowerCase().includes(
                q.toLowerCase())
                ||
                Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                    q.toLowerCase())
                    ||
                Sectors.get(item.sector).toLowerCase().includes(
                    q.toLowerCase())
                ||
               Aspect.get(item.aspect).toLowerCase().includes(
                    q.toLowerCase())
                    ||
                    item.aim.toLowerCase().includes(
                        q.toLowerCase())
                || item.description.toLowerCase().includes(q.toLowerCase())
        }
    }
    else if(Potentialuser.get(item.ideaPotentialUsers)===filterParampotentialuser && Aspect.get(item.aspect)===filterParamaspect){
        return item.title.toLowerCase().includes(
            q.toLowerCase())
            ||
            Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                q.toLowerCase())
                ||
            Sectors.get(item.sector).toLowerCase().includes(
                q.toLowerCase())
            ||
           Aspect.get(item.aspect).toLowerCase().includes(
                q.toLowerCase())
                ||
                item.aim.toLowerCase().includes(
                    q.toLowerCase())
            || item.description.toLowerCase().includes(q.toLowerCase())
    }
    else if(filterParampotentialuser==='All' && Aspect.get(item.aspect)===filterParamaspect){
        return item.title.toLowerCase().includes(
            q.toLowerCase())
            ||
            Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                q.toLowerCase())
                ||
            Sectors.get(item.sector).toLowerCase().includes(
                q.toLowerCase())
            ||
           Aspect.get(item.aspect).toLowerCase().includes(
                q.toLowerCase())
                ||
                item.aim.toLowerCase().includes(
                    q.toLowerCase())
            || item.description.toLowerCase().includes(q.toLowerCase())
    }
    else if(Potentialuser.get(item.ideaPotentialUsers)===filterParampotentialuser && filterParamaspect==='All'){
        return item.title.toLowerCase().includes(
            q.toLowerCase())
            ||
            Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                q.toLowerCase())
                ||
            Sectors.get(item.sector).toLowerCase().includes(
                q.toLowerCase())
            ||
           Aspect.get(item.aspect).toLowerCase().includes(
                q.toLowerCase())
                ||
                item.aim.toLowerCase().includes(
                    q.toLowerCase())
            || item.description.toLowerCase().includes(q.toLowerCase())
    }
    else if(filterParampotentialuser==='All'&&filterParamaspect==='All')
    {
        return item.title.toLowerCase().includes(
            q.toLowerCase())
            ||
            Potentialuser.get(item.ideaPotentialUsers).toLowerCase().includes(
                q.toLowerCase())
                ||
            Sectors.get(item.sector).toLowerCase().includes(
                q.toLowerCase())
            ||
           Aspect.get(item.aspect).toLowerCase().includes(
                q.toLowerCase())
                ||
                item.aim.toLowerCase().includes(
                    q.toLowerCase())
            || item.description.toLowerCase().includes(q.toLowerCase())
    }
        }
      );
  }
    if (error) {
        return (
            <p>
                {error.message}
            </p>
        );
    } else if (!isLoaded) {
        return <>loading...</>;
    } else {
        return (
            <div>
            
              <div className="PersonalIdea_Border">
              <div className="PersonalIdea_Titel">
              <LiaArrowLeftSolid className="Pfeilbackpersonalidea" onClick={()=>navi('/personaluser')}/>
              <Text className="PersonalIdea_Text"> <FaLightbulb className="PersonalIdea_icon"> </FaLightbulb>What are my Ideas</Text>
              </div>
                <div>
                    <label>
                        <input type="search"
                            name="search-form"
                            id="search-form"
                            className="search-input"
                            placeholder="Search for..."
                            value={q}
                            onChange={(e) => setQ(e.target.value)}
                        />
                        <select
                            onChange={(e) => {
                                setFilterParamaspect(e.target.value);
                            }}
                            className="idea_innovativeselect"
                            aria-label="Filter Countries By Innovative Aspect"
                        >
                           <option value="" disabled selected>Innovative Aspect</option>
                           <option value="All">All Innovative aspect</option>
                           <option value="Technology">Technology</option>
                           <option value="Project Management">Project Management</option>
                           <option value="New industry">New industry</option>
                          <option value="Focus topic">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
                        </select>
                        <select
                            onChange={(e) => {
                                setFilterParampotentialuser(e.target.value);
                            }}
                            onClick={(e)=>state(e.target.value)}
                            className="Potentialuserselect"
                            aria-label="Filter Countries By Sector & Industry"
                        >
                           <option value="" disabled selected>Potential users</option>
                           <option value="All">All Potential User</option>
                           <option value="Internally">Internally</option>
                           <option value="Customer">Customer</option>
                           <option value="Sector" >Sector</option>
                           </select>
                           {show ? 
                            <select  
                            onChange={(e) => {
                                setFilterParamasector(e.target.value);
                            }}className="idea_sectorselect">
                            <option value="" disabled selected>Sector & Industry</option>
                            <option value="Banking">Banking</option>
                            <option value="Defence & Intelligence">Defence & Intelligence</option>
                            <option value="Energy & Utilities">Energy & Utilities</option>
                            <option value="Health">Health</option>
                            <option value="Insurance">Insurance</option>
                            <option value="Life Sciences">Life Sciences</option>
                            <option value="Manufacturing">Manufacturing</option>
                            <option value="Media">Media</option>
                            <option value="Public">Public</option>
                            <option value="Retail & Consumer Services">Retail & Consumer Services</option>
                            <option value="Space">Space</option>
                            <option value="Telecommunication">Telecommunication</option>
                            <option value="Transportation & Logistics">Transportation & Logistics</option>
                            </select>:null}
                        </label>
                </div>
                <table className="PersonalIdea_table" >
                <thead className="thead">
                <tr className="tr">
                <th className="th1">Title</th>
                <th className="th2">Innovative Aspect</th>
                <th className="th3">Potential Users</th>
                <th className="th4">Sector</th>
                <th className="th5">Aim</th>
                <th className="th6">Description</th>
                <th className="th7">ContactPerson</th>
                <th className="th8">View/Edit</th>
                </tr>
                </thead>
                {
                    searchidea(items).map((item) => (
                        <tbody className="tbody"> 
                        <td>{item.title}</td>
                        <td>{Aspect.get(item.aspect)}</td>
                        <td>{Potentialuser.get(item.ideaPotentialUsers) }</td>
                        <td>{Sectors.get(item.sector)}</td>
                        <td>{item.aim}</td>
                        <td>{item.description}</td>
                        <td>{item.contactPerson}</td>
                        <td><button className="View" onClick={()=>navi('/projectview')}>View
                        </button>
                        /
                        <button className="Edit" onClick={()=>{Edit(item.idnr)}}>Edit
                        </button></td>
                        </tbody>
                    )
                    )}
                </table>
            </div>
            </div>
        );
    }
}
  