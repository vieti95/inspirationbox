import axios from "axios";
import {useEffect, useState} from "react";

const baseURL = "http://localhost:5248/api/Project";

export function Post(props) {
    const [post, setPost] = useState([]);
    const[title, setTitle]=useState('');
    const [creator, setBody]=useState('');
    useEffect(() => {
    axios.get(baseURL).then((response) => {
      console.log(response.data);
      setPost(response.data);
    });
  
  
  }, []);
  const createPost= (e) =>{
    e.preventDefault();
    axios
      .post(baseURL, {
        title,
        creator
      })
      .then((response) => console.log('Posting data', response))
      .catch(err=>console.log(err))
      
  }


  if (!post) return null;


  return (
    <div>
        <form>
      Title:<input type="text" value={title} onChange={(e)=>setTitle(e.target.value)}></input>
      Body:<input type="text" value={creator} onChange={(e)=>setBody(e.target.value)}></input>
      <button onClick={createPost}>submit</button>
      </form>
    </div>
  );
}