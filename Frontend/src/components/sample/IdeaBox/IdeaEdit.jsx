import axios from "axios";
import "./IdeaEdit.css";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams} from "react-router-dom";
import { Text } from "@fluentui/react-components";
import {useRef} from 'react';
import { LiaArrowLeftSolid} from "react-icons/lia";
import ReactModal from 'react-modal';

export function IdeaEdit(props) {
  let navigate = useNavigate();
  
  const titleRef = useRef(null);
    const aspectRef = useRef(null);
    const sectorRef=useRef(null);
    const descriptionRef = useRef(null);
    const ideaPotentialUsersRef = useRef(null);
    const aimRef = useRef(null);
  const { id } = useParams();
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useState({});
  const [show,setShow]=useState(false);

  const { title, sector, aspect, description, contactPerson,ideaPotentialUsers, aim } = user;
 
  const EditDelet =()=>
  {
    axios.delete(`http://localhost:5248/api/Idea/${id}`)
   .then((response)=>{
    console.log(response)
    console.log("delete")
    navigate(-1)
   })
   
  }
  
  const Update=()=>{
    setIsOpen(false)
    navigate(-1)
  }

  const onInputChange = (e) => {

    setUser({ ...user, [e.target.name]: e.target.value });
   
  };

  useEffect(() => {
    loadUser();

  }, []);

    const onSubmit = (e) => {
      console.log(titleRef.current.value);
      console.log(ideaPotentialUsersRef.current.value);
      console.log(aimRef.current.value);
      console.log(descriptionRef.current.value);
      console.log(aspectRef.current.value);
      console.log(sectorRef.current.value);
      var intpotentialuser= parseInt(ideaPotentialUsersRef.current.value);
      var intaspect= parseInt(aspectRef.current.value);
      var intsector= parseInt(sectorRef.current.value);
    e.preventDefault();
    axios.put(`http://localhost:5248/api/Idea/${id}`,{
      title: titleRef.current.value,
      ideaPotentialUsers: intpotentialuser,
      aspect: intaspect,
      sector: intsector,
      useridnr: 1,
      aim: aimRef.current.value,
      description: descriptionRef.current.value,
  })
    .then((res)=>{
      console.log(res)
      console.log("erfolgreich")
      setIsOpen(true)  
     })
     .catch(function (error) {
      console.log(error);
     
    })
  };

  const loadUser = async() => {
    const result =await axios.get(`http://localhost:5248/api/Idea/${id}`);
    if(result.sector===13)
    {
      setShow(currentShow => !currentShow)
    }
    setUser(result.data);
    
  };


  return (
      <div className="Idea_Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navigate('/ideabox')}/>
      <div className="Edit_Title">
      <Text className="Text">{title}</Text>
      </div>
     

          <form onSubmit={(e) => onSubmit(e)}>
            <div className="EditAddtitle">
              <label className="Text">
                Project Title
              </label>
              <input
                type={"text"}
                className="Field"
                name="title"
                value={title}
                ref={titleRef}
                onChange={(e)=>onInputChange(e)}
              />

<div className="EditInnovative">
                        <label className="Text">
                         Innovative aspect
                        </label>
                        <select
                            name="aspect"
                            value={aspect}
                            ref={aspectRef}
                            onChange={(e) => onInputChange(e)}
                            className="select"
                        >
                          <option value="" disabled selected>Innovative aspect</option>
      <option type="number" value="0">Technology</option>
      <option type="number" value="1">Project Management</option>
      <option type="number" value="2">New industry</option>
      <option type="number" value="3">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
                        </select>
                </div>

            </div>
            <div className="EditPotentialuser">
            <label className="Text">
                Potential User
              </label>
            <select
           
                            name="ideaPotentialUsers"
                            value={ideaPotentialUsers}
                            ref={ideaPotentialUsersRef}
                            onChange={(e)=>onInputChange(e)}
                            className="select"
                        >
                            <option type="number" value="" disabled selected>Potential users</option>
                           <option type="number" value="0">Internally</option>
                           <option type="number" value="1">Customer</option>
                           <option type="number" value="2" >Sector</option>
                        </select>
                        </div>
            
            <div className="EditSector">
            <label className="Text">
                Sector & Industry
              </label>
              
            <select 
           defaultValue="" disabled={show}
                            name="sector"
                            value={sector}
                            onChange={(e) => onInputChange(e)}
                            ref={sectorRef}
                            className="select"
                            aria-label="Filter Countries By Sector & Industry"
                            
                        >
                            <option value="" disabled selected>Sector & Industry</option>
      <option type="number" value="0">Banking</option>
      <option type="number" value="1">Defence & Intelligence</option>
      <option type="number" value="2">Energy & Utilities</option>
      <option type="number" value="3">Health</option>
      <option type="number" value="4">Insurance</option>
      <option type="number" value="5">Life Sciences</option>
      <option type="number" value="6">Manufacturing</option>
      <option type="number" value="7">Media</option>
      <option type="number" value="8">Public</option>
      <option type="number" value="9">Retail & Consumer Services</option>
      <option type="number" value="10">Space</option>
      <option type="number" value="11">Telecommunication</option>
      <option type="number" value="12">Transportation & Logistics</option>
      <option type="number" value="13">Nothing</option>
                        </select>
                        </div>
                        
            <div className="EditAim">
              <label className="Text">
                Aim
              </label>
              <input
                type={"text"}
                className="Field"
                name="aim"
                value={aim}
                ref={aimRef}
                onChange={(e)=>onInputChange(e)}
              />
              </div>
                <div className="EditDescription">
                      <label className="Text">
                       Short Description
                       </label>
                       <input
                       type={"text"}
                className="Field"
                name="description"
                value={description}
                ref={descriptionRef}
                onChange={(e) => onInputChange(e)}
              />
            </div>

            <div className="EditContact">
              <label  className="Text">
                Contact Person
              </label>
              <input
                type={"text"}
                className="Field"
                placeholder="Enter your e-mail address"
                name="contactPerson"
                value={contactPerson}
                onChange={(e) => onInputChange(e)}
              />
            </div>
            
            <div className="EditSubmit">
            <button  type="submit" className="SubmitButton">
              Submit
            </button>
            {isOpen&& (
                 
                 <ReactModal className="Popup"
                 isOpen={isOpen}
               >
                 <div className="Edit_Success">
                 <p className="Titeltext">{titleRef.current.value}</p> 
                <p className="Text">Success Submitted</p>
                 
                 <button className="Button" onClick={()=>Update()}>Close</button>
                 </div>
               </ReactModal>    
               )}
            </div>   
            </form>
            <div className="EditDelete">
            <button className="DeleteButton" onClick={()=>EditDelet()} >
              Delete
            </button>
            </div> 
        </div>
    
  );
}