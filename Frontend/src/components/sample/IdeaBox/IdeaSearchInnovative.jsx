import "./IdeaSearchInnovative.css";
import { PiShareNetworkBold } from "react-icons/pi";
import { GiBrain } from 'react-icons/gi';
import { AiFillHome } from "react-icons/ai";
import { AiOutlineFundProjectionScreen } from "react-icons/ai";
import FactoryIcon from '@mui/icons-material/Factory';
import {  Link } from "react-router-dom";
import { Text } from "@fluentui/react-components";
import {  useNavigate } from "react-router-dom";
import { PiCubeFocusDuotone } from "react-icons/pi";
import {
  CircleMenu,
  CircleMenuItem,
 
} from "react-circular-menu";
import { FaLightbulb } from 'react-icons/fa';
import { LiaArrowLeftSolid} from "react-icons/lia";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

export function IdeaSearchInnovative(props){
  const navi =useNavigate();

  return (
      <div className="Idea_Border">
        <div className="Idea_Titel">
        <Text className="Idea_SearchText"> <FaLightbulb className="Idea_icon"> </FaLightbulb> Search by Innovative</Text>
        <LiaArrowLeftSolid className="PfeilbackIdeaSearch" onClick={()=>navi('/ideabox')}/>
        </div>
        <div className="Idea_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Idea_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Idea_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Idea_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Idea_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "90vh",
        }}
      >
        
        <CircleMenu
          startAngle={-90}
          rotationAngle={360}
          itemSize={3}
          radius={9}
         
          rotationAngleInclusive={false}
          className={"asean-symbol"}
        >
          
          <CircleMenuItem
          tooltip="Technology"  
          onClick={()=>navi('/technology')}
          >
              < PiShareNetworkBold className=" PiShareNetworkBold"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Project Management"  
          onClick={()=>navi('/projectmanager')}
          >
              < AiOutlineFundProjectionScreen className="AiOutlineFundProjectionScreen"/>
          </CircleMenuItem>
          
          <CircleMenuItem
          tooltip="New Industry"  
          onClick={()=>navi('/newindustry')}
          >
              < FactoryIcon className="FactoryIcon"/>
          </CircleMenuItem>

          <CircleMenuItem
          tooltip="Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])"  
          onClick={()=>navi('/focustopic')}
          >
              < PiCubeFocusDuotone className="PiCubeFocusDuotone"/>
          </CircleMenuItem>
        </CircleMenu>
      </div>
      </div>
    
  );
}
      