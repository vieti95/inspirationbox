import React from "react";
import "./IdeaView.css";
import { Text } from "@fluentui/react-components";
import { useState, useEffect} from "react";
import { useNavigate, useParams} from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";
import axios from "axios";

export function IdeaView(props)
{
  const [iData, setData] = useState([]);
  const { title, sector, aspect, description, contactPerson } = iData;
  const navi = useNavigate();
  const { id } = useParams();
  useEffect(() => {
    loadUser();

  }, []);
  const loadUser = async() => {
    const result =await axios.get(`http://localhost:5248/api/Idea/${id}`);
    setData(result.data);

  };

 
  return(
    <div>
      <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/ideabox')}/>
      <div className="View_Title">
      <Text className="Text">{title}</Text>
      </div>  
      
      
    </div>
      
    </div>
  );
}