import "./IdeaAdd.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi'
import { IoIosAddCircle } from "react-icons/io";
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import axios from "axios";
import {useRef} from 'react';
import { useState} from "react";
import ReactModal from 'react-modal';
import { FaLightbulb } from 'react-icons/fa';
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

const baseURL = "http://localhost:5248/api/Idea";

export function IdeaAdd(props)
{
    const [message, setMessage] = useState(null);
    const Idea_titleRef = useRef(null);
    const Idea_aspectRef = useRef(null);
    const Idea_sectorRef=useRef(null);
    const Idea_descriptionRef = useRef(null);
    const Idea_ideaPotentialUsersRef = useRef(null);
    const Idea_aimRef=useRef(null);
    const contactRef = useRef(null);
    const [isOpen, setIsOpen] = useState(false);
    const [show,setShow]=useState(false);
    const navi =useNavigate();

  const state=(e)=>{
   if(e==="2"){
    setShow(currentShow => !currentShow)
   } 
  }
  const Close=()=>{
    setIsOpen(false)
    navi(-1)
  }
    
  return(
    <div>
      <div className="Idea_Border">
       <div className="Idea_Titel">
      <Text className="Idea_IdeaAddText"> <FaLightbulb className="Idea_icon"> </FaLightbulb> Add a Idea</Text>
      <LiaArrowLeftSolid className="PfeilbackIdeaAdd" onClick={()=>navi('/ideabox')}/>
      </div>
      <div className="Idea_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Idea_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Idea_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Idea_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Idea_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
     <div className="Idea">
      <IoIosAddCircle className="Idea_icon"></IoIosAddCircle>
      </div>
      <div className="Idea_Add">
      <input ref={Idea_titleRef} type="text" className="titleText" placeholder="Idea title" size="52"></input>
      </div>
      <div className="Idea_Potentialuser">
      <select onClick={(e)=>state(e.target.value)} ref={Idea_ideaPotentialUsersRef} className="select" name="Potentialuser" placeholder="Potential User">
      <option value="" disabled selected>Potential users</option>
      <option type="number" value="0">Internally</option>
      <option type="number" value="1">Customer</option>
      <option type="number" value="2">Sector</option>
      </select>
      </div>
      <div className="Idea_addSector">
      {show ? 
      <select className="select" name="Sector" placeholder="Sector" ref={Idea_sectorRef}>
      <option value="" disabled selected>Sector & Industry</option>
      <option type="number" value="0">Banking</option>
      <option type="number" value="1">Defence & Intelligence</option>
      <option type="number" value="2">Energy & Utilities</option>
      <option type="number" value="3">Health</option>
      <option type="number" value="4">Insurance</option>
      <option type="number" value="5">Life Sciences</option>
      <option type="number" value="6">Manufacturing</option>
      <option type="number" value="7">Media</option>
      <option type="number" value="8">Public</option>
      <option type="number" value="9">Retail & Consumer Services</option>
      <option type="number" value="10">Space</option>
      <option type="number" value="11">Telecommunication</option>
      <option type="number" value="12">Transportation & Logistics</option>
      </select>:null}
      </div>
      <div className="Idea_Innovative">
      <select ref={Idea_aspectRef} className="select" name="Innovative" placeholder="Innovative aspect">
      <option value="" disabled selected>Innovative aspect</option>
      <option type="number" value="0">Technology</option>
      <option type="number" value="1">Project Management</option>
      <option type="number" value="2">New industry</option>
      <option type="number" value="3">Focus topic (agile project mgmt.; green IT; IT security; AI [Public Sector])</option>
      </select>
      </div>
      <div className="Idea_Aim">
      <input ref={Idea_aimRef} type="text" className="AimText" placeholder="Aim" size="52"></input>
      </div>
      <div className="Idea_shortdescription">
      <input ref={Idea_descriptionRef} type="text" className="shortdescriptionText" placeholder="Short Description" size="52"></input>
      </div>
      <div className="Idea_Submit">
                {show?
                <button className="Button" onClick={() => {
                    console.log(Idea_titleRef.current.value);
                    console.log(Idea_ideaPotentialUsersRef.current.value);
                    console.log(Idea_aimRef.current.value);
                    console.log(Idea_descriptionRef.current.value);
                    console.log(Idea_aspectRef.current.value);
                    console.log(Idea_sectorRef.current.value);
                    var intpotentialuser= parseInt(Idea_ideaPotentialUsersRef.current.value);
                    var intaspect= parseInt(Idea_aspectRef.current.value);
                    var intsector= parseInt(Idea_sectorRef.current.value);
                    console.log(intpotentialuser);
                    axios.post(baseURL, {
                      title: Idea_titleRef.current.value,
                      ideaPotentialUsers: intpotentialuser,
                      aspect: intaspect,
                      sector: intsector,
                      useridnr: 1,
                      aim: Idea_aimRef.current.value,
                      description: Idea_descriptionRef.current.value,
                    })
                 
                    .then(function (response) {
                      console.log(response);
                      //setPost(response.data);
                      setMessage({type: 'success', response});
                      setIsOpen(true)
                      
                    })
                    .catch(function (error) {
                      console.log(error);
                      setMessage({type:'error', error});
                    });
      
              }} >Submit</button> :
              <button className="Button" onClick={() => {
                console.log(Idea_titleRef.current.value);
                console.log(Idea_ideaPotentialUsersRef.current.value);
                console.log(Idea_aimRef.current.value);
                console.log(Idea_descriptionRef.current.value);
                console.log(Idea_aspectRef.current.value);
                
                var intpotentialuser= parseInt(Idea_ideaPotentialUsersRef.current.value);
                var intaspect= parseInt(Idea_aspectRef.current.value);
               
                console.log(intpotentialuser);
                axios.post(baseURL, {
                  title: Idea_titleRef.current.value,
                  ideaPotentialUsers: intpotentialuser,
                  aspect: intaspect,
                  sector: 13,
                  useridnr: 1,
                  aim: Idea_aimRef.current.value,
                  description: Idea_descriptionRef.current.value,
                })

                .then(function (response) {
                  console.log(response);
                  //setPost(response.data);
                  setMessage({type: 'success', response});
                  setIsOpen(true)
                  
                })
                .catch(function (error) {
                  console.log(error);
                  setMessage({type:'error', error});
                });
  
          }} >Submit</button>
            }
            <div className="Alert">
              {message?.type==='error'&&(alert("Please fill in all fields "))}
              </div>
              {isOpen&& (
                 
                <ReactModal className="Idea_Popup"
                isOpen={isOpen}
              >
                <div className="Idea_Success">
                <p className="Titeltext">{Idea_titleRef.current.value}</p> 
                <p className="Text">Success Submitted</p>
                
                <button className="Button" onClick={()=>Close()}>Close</button>
                </div>
              </ReactModal>    
              )}
   </div>  
      </div>
      </div>
 );
}