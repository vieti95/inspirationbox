import "./Homepage.css";
import { GiBrain } from 'react-icons/gi'
import { FaLightbulb } from 'react-icons/fa'
import { BsRainbow } from 'react-icons/bs'
import { Text } from "@fluentui/react-components";
import {  useNavigate } from "react-router-dom";
import PersonIcon from '@mui/icons-material/Person';
import {  Link } from "react-router-dom";
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { AiFillHome } from "react-icons/ai";

export function Homepage(props) {
const navigate = useNavigate();
  return (
    <div>
            <div className="Homepage_Border">
            <div className="Home_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Home_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Home_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Home__Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Home_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Home_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
          <div className="Home_ideabox"> 
          
      <button className="button" onClick={()=>navigate('/ideabox')}> <FaLightbulb className="icon"></FaLightbulb> 
      <Text className="text">Idea Box</Text>
      </button>
    </div>
          <div className="Home_Projectbox"> 
          <button className="button" onClick={()=>navigate('/projectbox')}> <GiBrain className="icon"> </GiBrain> 
          <Text className="text"> Project Box </Text> 
          </button>
          </div>
          <div className="Home_Inspirationbox"> 
          <button className="button" onClick={()=>navigate('/inspiration')}> <BsRainbow className="icon"></BsRainbow> 
          <Text className="text"> Inspiration Box </Text> 
          </button>
          </div>
          </div>
          </div>
        
  );
}
