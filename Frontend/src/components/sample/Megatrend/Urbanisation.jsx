import "./Urbanisation.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { BiSolidCity } from "react-icons/bi";

export function Urbanisation(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Urbanisation_Title">
      <Text className="Text"> <BiSolidCity className="Urbanisation_icon"></BiSolidCity>Urbanisation</Text>
      </div>
     
    </div>
  );
}