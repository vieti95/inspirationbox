import "./Gender-Shift.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import TransgenderIcon from '@mui/icons-material/Transgender';

export function GenderShift(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="GenderShift_Title">
      <Text className="Text"> <TransgenderIcon className="GenderShift_icon" fontSize="large"></TransgenderIcon> Gender Shift</Text>
      </div>
     
    </div>
  );
}