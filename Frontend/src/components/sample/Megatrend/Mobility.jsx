import "./Mobility.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { FaLocationDot } from "react-icons/fa6";

export function Mobility(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Mobility_Title">
      <Text className="Text"> <FaLocationDot className="Mobility_icon"></FaLocationDot>Mobility</Text>
      </div>
     
    </div>
  );
}