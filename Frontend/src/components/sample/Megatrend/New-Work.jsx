import "./New-Work.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { MdWork } from "react-icons/md";

export function NewWork(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="NewWork_Title">
      <Text className="Text"> <MdWork className="NewWork_icon"></MdWork>New Work</Text>
      </div>
     
    </div>
  );
}