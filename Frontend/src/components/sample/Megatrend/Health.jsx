import "./Health.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import HealingIcon from '@mui/icons-material/Healing';

export function Healthtrend(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Healthtrend_Title">
      <Text className="Text"><HealingIcon fontSize="large"></HealingIcon>Health</Text>
      </div>
     
    </div>
  );
}