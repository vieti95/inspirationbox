import "./Connectivity.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { FaConnectdevelop } from "react-icons/fa";

export function Connectivity(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Connectivity_Title">
      <Text className="Text"> <FaConnectdevelop className="Connectivity_icon"></FaConnectdevelop> Connectivity</Text>
      </div>
     
    </div>
  );
}
