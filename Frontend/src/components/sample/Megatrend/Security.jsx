import "./Security.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import SecurityIcon from '@mui/icons-material/Security';

export function Security(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Security_Title">
      <Text className="Text"> <SecurityIcon fontSize="large"></SecurityIcon>Security</Text>
      </div>
     
    </div>
  );
}