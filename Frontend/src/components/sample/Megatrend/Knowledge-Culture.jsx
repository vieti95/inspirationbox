import "./Knowledge-Culture.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";

export function KnowledgeCulture(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="KnowledgeCulture_Title">
      <Text className="Text">Knowledge Culture</Text>
      </div>
     
    </div>
  );
}