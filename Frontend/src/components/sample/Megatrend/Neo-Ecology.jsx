import "./Neo-Ecology.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";

export function NeoEcology(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="NeoEcology_Title">
      <Text className="Text">Neo Ecology</Text>
      </div>
     
    </div>
  );
}