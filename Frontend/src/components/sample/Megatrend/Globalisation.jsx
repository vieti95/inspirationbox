import "./Globalisation.css";
import React from "react";
import { Text } from "@fluentui/react-components";
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import PublicIcon from '@mui/icons-material/Public';

export function Globalisation(props) {
 
    const navi = useNavigate();

  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/inspiration')}/>
      <div className="Globalisation_Title">
      <Text className="Text"><PublicIcon fontSize="large"></PublicIcon>Globalisation</Text>
      </div>
     
    </div>
  );
}