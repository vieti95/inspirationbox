import React from 'react';
import { useMediaQuery } from 'react-responsive';
import MediaQuery from 'react-responsive'

export function Get(props) {
  


  const [isDisabled, setIsDisabled] = React.useState(false);

  const handleClick = () => {
    setIsDisabled(!isDisabled)
  }
  
  return (
    <div className='App'>
      <select defaultValue="" disabled={isDisabled}>
        <option value="">choose an option</option>
        <option value="one">1</option>
        <option value="two">2</option>
        <option value="three">3</option>
        <option value="four">4</option>
      </select>

      <button onClick={handleClick}>Click Me</button>
    </div>
  );
}