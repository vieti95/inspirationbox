import "./InspirationProject.css";
import ImageZoom from "react-image-zooom";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi'
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import { FaLightbulb } from 'react-icons/fa';
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

export function InspirationProject(props) {

  const navi =useNavigate();


  return (
    <div className="App">
      <div className="Inspiration_Border">
       <div className="Inspiration_Titel">
      <Text className="Inspiration_Text"> <BsRainbow className="Inspiration_icon"> </BsRainbow>InspirationBox</Text>
      <LiaArrowLeftSolid className="Pfeilbackinspiration" onClick={()=>navi('/ideabox')}/>
      </div>
      <div className="Inspiration_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="Inspiration_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="Inspiration_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="Inspiration_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="Inspiration_faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
     <h2 className="Titeltrends">Check out the new normal</h2>
     <h3 className="Megatrendtitel">Mega Trends</h3>
      <div className="trendmaps">
                <ImageZoom
                  src={"https://www.gesundheitsmanagerin.at/images/2018/12/18/5_1-Megatrend-Map-zukunftsinstitut-Nov16.jpg"}
                  alt="Zoom-images"
                  zoom="300"
                  width="750"
                  height="520"
                />
      </div>
      <div className="Megatrend">
        <p><Link className="Gender-Shift" to="/gendershift">Gender Shift</Link></p>
        <p><Link className="Healthtrend" to="/healthtrend">Health</Link></p>
        <p><Link className="Globalisation" to="/globalisation">Globalisation</Link></p>
        <p><Link className="Connectivity" to="/connectivity">Connectivity</Link></p>
        <p><Link className="Individualisation" to="/individualisation">Individualisation</Link></p>
        <p><Link className="Mobility" to="/mobility">Mobility</Link></p>
        <p><Link className="Security" to="/security">Security</Link></p>
        <p><Link className="New-Work" to="/newwork">New Work</Link></p>
        <p><Link className="Neo-Ecology" to="/neoecology">Neo-Ecology</Link></p>
        <p><Link className="Urbanisation" to="/urbanisation">Urbanisation</Link></p>
        <p><Link className="Silver-Age" to="/silverage">Silver Age</Link></p>
        <p><Link className="Knowledge-Culture" to="/knowledgeculture">Knowlege Culture</Link></p>
      </div>
     </div>
      </div>

  );
};
