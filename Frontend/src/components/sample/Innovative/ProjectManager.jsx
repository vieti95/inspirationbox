import "./ProjectManager.css";
import React from "react";
import { useState,useEffect } from "react";
import axios from "axios";
import { Text } from "@fluentui/react-components";
import {  useNavigate } from "react-router-dom";
import { LiaArrowLeftSolid} from "react-icons/lia";

export function ProjectManager(props) {
  const [iGet, setGet] = useState([]);
 const navi = useNavigate();
 const Sectors = new Map();
  const Aspect= new Map();
  const Potentialuser= new Map();


  Sectors.set(0, "Banking");
  Sectors.set(1,"Defence & Intelligence")
  Sectors.set(2,"Energy & Utilities")
  Sectors.set(3,"Health")
  Sectors.set(4,"Insurance")
  Sectors.set(5,"Life Sciences")
  Sectors.set(6,"Manufacturing")
  Sectors.set(7,"Media")
  Sectors.set(8,"Public")
  Sectors.set(9,"Retail & Consumer Services")
  Sectors.set(10,"Space")
  Sectors.set(11,"Telecommunication")
  Sectors.set(12,"Transportation & Logistics")
  Sectors.set(13,"Nothing")

  Aspect.set(1,"Project Management")

  Potentialuser.set(0,"Internally")
  Potentialuser.set(1,"Customer")
  Potentialuser.set(2,"Sector")
  useEffect(() => {
    axios
        .get("http://localhost:5248/api/Idea/")
        .then((response) => {
          setGet(response.data);
            console.log(response);
        })
        .catch((err) => console.log(err));
}, []);

const Edit=(id)=>{
  navi('/ideaedit/'+id)
}

  const renderTable = () => {
    return iGet.map(user => {
     
        if(user.aspect===1)
        {
          return(
          
            <tr>
            <td>{user.title}</td>
            <td>{Aspect.get(user.aspect)}</td>
            <td>{Potentialuser.get(user.ideaPotentialUsers) }</td>
            <td>{Sectors.get(user.sector)}</td>
            <td>{user.aim}</td>
            <td>{user.description}</td>
            <td>{user.contactPerson}</td>
            <td><button className="View" onClick={()=>navi('/projectview')}>View
            </button>
            /
            <button className="Edit" onClick={()=>{Edit(user.idnr)}}>Edit
            </button></td>
            </tr>
                  )
        }
      
      })
  }



  return (
    <div className="Border">
      <LiaArrowLeftSolid className="Pfeilback" onClick={()=>navi('/ideasearchinnovative')}/>
      <div className="ProjectManager_Title">
      <Text className="Text">Project Manager</Text>
      </div>
     <table className="Sector_table" >
          <thead className="thead">
          <tr className="tr">
          <th className="th1">Title </th>
          <th className="th2">Innovative Aspect</th>
          <th className="th3">Potential Users</th>
          <th className="th4">Sector</th>
          <th className="th5">Aim</th>
          <th className="th6">Description</th>
          <th className="th7">ContactPerson</th>
          <th className="th8">View/Edit</th>
         </tr>
         </thead>
         <tbody className="tbody">
         {renderTable()}
          </tbody>
         </table>
    </div>
  );
}
