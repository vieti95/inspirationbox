import "./faq.css";
import { Text } from "@fluentui/react-components";
import { GiBrain } from 'react-icons/gi'
import {  Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import { FaLightbulb } from 'react-icons/fa';
import { LiaArrowLeftSolid} from "react-icons/lia";
import {  useNavigate } from "react-router-dom";
import { BsRainbow } from 'react-icons/bs';
import PersonIcon from '@mui/icons-material/Person';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { FiPlus } from "react-icons/fi";
import React, { useState, useRef, useEffect } from "react";



export function FAQ(props)
{
    const navi = useNavigate();
    const [active, setActive] = useState(false);

  const contentRef = useRef(null);

  useEffect(() => {
    contentRef.current.style.maxHeight = active
      ? `${contentRef.current.scrollHeight}px`
      : "0px";
  }, [contentRef, active]);


  const toggleAccordion = () => {
    setActive(!active);
  };



  return(
    <div>
      <div className="faq_Border">
       <div className="faq_Titel">
      <Text className="faq_Text"> <QuestionAnswerIcon className="faq_icon"> </QuestionAnswerIcon> FAQ</Text>
      <LiaArrowLeftSolid className="Pfeilbackfaq" onClick={()=>navi(-1)}/>
      </div>
      <div className="Faq_Menue">
     <Link className="Home" to="/homepage"><AiFillHome className="Homeicon"></AiFillHome></Link>
     <p ><Link className="Project" to="/projectbox"><GiBrain className="faq_Projecticon"> </GiBrain></Link></p>
     <p ><Link className="Idea" to="/ideabox"><FaLightbulb className="faq_Ideaicon"> </FaLightbulb></Link></p>
     <p ><Link className="Inspiration" to="/inspiration"><BsRainbow className="faq_Inspirationicon"> </BsRainbow></Link></p>
     <p ><Link className="PersonalUser" to="/personalUser"><PersonIcon className="faq_PersonalUsericon"> </PersonIcon></Link></p>
     <p ><Link className="FAQ" to="/faq"><QuestionAnswerIcon className="faqicon"> </QuestionAnswerIcon></Link></p>
     </div>
     <div>
        <div className="ProjectBox1">
        <h2 className="Titel">FAQ</h2>
          <button
            className={`projectquestion ${active}`}
            onClick={toggleAccordion}
          >
            
              <div className="projectalign">
                <h4 className="projectstyle">
                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed tempor sem. Aenean vel turpis feugiat Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed tempor sem. Aenean vel turpis feugiat Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed tempor sem. Aenean vel turpis feugiat
                </h4>
                <FiPlus
                  className={active ? `projecticon projectroate` : `projecticon`}
                />
              </div>
              <div
                ref={contentRef}
                className={active ? `projectanswer projectanswerdivider` : `projectanswer`}
              >
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed tempor sem. Aenean vel turpis feugiat,
            ultricies metus at, consequat velit. Curabitur est nibh, varius in tellus nec, mattis pulvinar metus.
            In maximus cursus lorem.</p>
              
            </div>
          </button>
          </div>


        </div>
     </div>
     </div>
     );
     }