using System.Text;
using System.Text.Json;
using InsboxService.DTO;

namespace InsboxService.SyncDataService.Http
{
    public class HttpCommandDataClient : ICommandDataClient
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public HttpCommandDataClient(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }

        public async Task SendProjectToCommand(ProjectReadDTO project)
        {
            var httpContent = new StringContent(JsonSerializer.Serialize(project), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{_configuration["CommandService"]}", httpContent);
        
            if( response.IsSuccessStatusCode)
                Console.WriteLine("Success Sync");
            else
                Console.WriteLine("Unsuccees Sync");
        
        }
    }
}