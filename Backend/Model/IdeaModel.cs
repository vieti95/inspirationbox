using System.ComponentModel.DataAnnotations;
using InsboxService.DTO;
public enum PotentialUser
{
    Internally, //0 to 3
    Customer,
    Sector
}
namespace InsboxService.Model
{
    public class IdeaModel
    {
        public IdeaModel(IdeaCreateDTO idea, Usermodel creator)
        {
            this.Title= idea.Title;
            this.Aspect= idea.Aspect;
            this.Description=idea.Description;
            this.useridnr = idea.useridnr;
            this.Sector= idea.Sector;
            this.Aim= idea.Aim;
            this.IdeaPotentialUsers= idea.IdeaPotentialUsers;
            this.Creator= creator;
        }
        public IdeaModel()
        {
            
        }

        [Key]
        [Required]
        public int Idnr { get; set; }

        [Required]
        public string Title { get; set; }

        public Usermodel Creator { get; set; }
        public int useridnr{get; set;} //nr of creator
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }

        public string Aim{ get; set; }
        public PotentialUser IdeaPotentialUsers{ get; set; }
        public string Description { get; set; }
        //public string/*Benutzer Obejekt*/ ContactPerson { get; set; }

        public ICollection<Comment> IdeaComments{get; set;}
        public ICollection<Evaluation> evaluations{get; set;}
        
    }
}