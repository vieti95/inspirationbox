using System.ComponentModel.DataAnnotations;

namespace InsboxService.Model
{
    public class Comment
    {

        [Key]
        [Required]
        public int Serialnr { get; set; }

        [Required]
        public string TextofComment { get; set; }

        [Required]
        public string Creator { get; set; }

        public ICollection<Evaluation> Evaluations{get; set;}
        
    }
}