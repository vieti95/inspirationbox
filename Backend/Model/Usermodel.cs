using System.ComponentModel.DataAnnotations;
using InsboxService.DTO;

namespace InsboxService.Model
{
    public class Usermodel


    {

        [Key]
        [Required]
        public int UserIdnr { get; set; }

        [Required]
        public string Email { get; set; }

        public string Name{ get; set; }

        public ICollection<ProjectModel> MyProjects{get; set;}
        
        public ICollection<IdeaModel> MyIdeas{get; set;}


        public ICollection<Evaluation> MyEvaluations{get; set;}
        
    }
}