using System.ComponentModel.DataAnnotations;
public enum Esector
{
    Banking, //0 to 12
    Defence_Intelligence,
    Energy_Utilities,
    Health,
    Insurance,
    Life_Sciences,
    Manufacturing,
    Media,
    Public,
    Retail_Consumer_Services,
    Space,
    Telecommunication,
    Transportation_Logistics,

    Nothing
}
public enum Easpect
{
    Technology, //0 to 3
    Project_management,
    New_industry,
    Focus_topic
}
namespace InsboxService.Model
{
    public class ProjectModel
    {

        [Key]
        [Required]
        public int Idnr { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public Usermodel Creator { get; set; }
        public int creatoridnr { get; set; }
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }
        public string Description { get; set; }
        public Usermodel ContactPerson { get; set; }
        public int contactpersonidnr { get; set; }
   
    }
}