using System.ComponentModel.DataAnnotations;

namespace InsboxService.Model
{
    public class Evaluation
    {

        [Key]
        [Required]
        public int Idnr { get; set; }
        public Usermodel User{get; set;}
        public int useridnr{get; set;}
        public int Likeqty { get; set; }
        public int Dislikeqty { get; set; }
    }
}