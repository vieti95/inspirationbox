using InsboxService.Model;

namespace InsboxService.Data
{
    public class ProjectRepo : IProjectRepo
    {
        private readonly AppDbContext _context;
        public ProjectRepo(AppDbContext context)
        {
            _context = context;
        }
        public void CreateProject(ProjectModel project)
        {
            if (project == null)
            {
                throw new ArgumentNullException(nameof(project));
            }
            _context.Projects.Add(project);
        }

        public IEnumerable<ProjectModel> GetAllProjects()
        {
            return _context.Projects.ToList();
        }

        public ProjectModel GetProjectById(int idnr)
        {
            return _context.Projects.FirstOrDefault(p => p.Idnr == idnr);
        }



        public ProjectModel DeleteProject(int projecktId)
        {
            var result = _context.Projects.FirstOrDefault(e => e.Idnr == projecktId);

            if (result != null)
            {
                _context.Projects.Remove(result);
                _context.SaveChanges();
                return result;
            }

            return null;
        }
        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public ProjectModel UpdateProject(ProjectModel project)
        {
            if (project == null)
            {
                throw new ArgumentNullException(nameof(project));
            }
            _context.Update(project);
            _context.SaveChanges();
            return _context.Projects.FirstOrDefault(p => p.Idnr == project.Idnr);
        }
    }
}