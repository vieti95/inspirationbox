using InsboxService.Model;
namespace InsboxService.Data
{
    public interface IProjectRepo
    {
        bool SaveChanges();
        IEnumerable<ProjectModel> GetAllProjects();
        ProjectModel GetProjectById(int idnr);
        void CreateProject(ProjectModel project);
        public ProjectModel DeleteProject(int projectId);
        ProjectModel UpdateProject(ProjectModel project);
    }
}