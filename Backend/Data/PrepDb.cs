using InsboxService.Model;

namespace InsboxService.Data
{
    public static class PrepDb
    {
        public static void PrepPop(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>());
            }
        }

        private static void SeedData(AppDbContext context)
        {

            if (!context.Users.Any())
            {
                context.Users.AddRange(
                   new Usermodel { Email = "test@cgi.com", Name = "Testname" }
                );

            }

            if (!context.Ideas.Any())
            {
                context.Ideas.AddRange(
                    new IdeaModel() { Title = "superIdea", Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" },Aim="AimTest", IdeaPotentialUsers= PotentialUser.Customer, Aspect = Easpect.New_industry, Description = "TestDesc", Sector = Esector.Nothing },
                                        new IdeaModel() { Title = "superIdea", Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" },Aim="AimTest", IdeaPotentialUsers= PotentialUser.Customer, Aspect = Easpect.New_industry, Description = "TestDesc", Sector = Esector.Public }


                );

            }
            if (!context.Projects.Any())
            {
                Console.WriteLine("Seeding Data");
                context.Projects.AddRange(



                    //context find User
                    new ProjectModel() { Title = "Test", ContactPerson = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" }, Aspect = Easpect.New_industry, Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" }, Description = "TestDesc", Sector = Esector.Media },
                    new ProjectModel() { Title = "Test2", ContactPerson = new Usermodel { Email = "khe@cgi.com", Name = "Khalil2" }, Aspect = Easpect.Technology, Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" }, Description = "TestDesc", Sector = Esector.Life_Sciences },
                    new ProjectModel() { Title = "Test3", ContactPerson = new Usermodel { Email = "khe@cgi.com", Name = "Khalil3" }, Aspect = Easpect.Project_management, Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" }, Description = "TestDesc", Sector = Esector.Banking },
                    new ProjectModel() { Title = "Test4", ContactPerson = new Usermodel { Email = "khe@cgi.com", Name = "Khalil4" }, Aspect = Easpect.Focus_topic, Creator = new Usermodel { Email = "khe@cgi.com", Name = "Khalil" }, Description = "TestDesc", Sector = Esector.Retail_Consumer_Services }
                );
                
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Daten schon da");
            }
        }
    }
}