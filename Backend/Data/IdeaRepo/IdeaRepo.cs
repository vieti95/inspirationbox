using InsboxService.Controller;
using InsboxService.DTO;
using InsboxService.Model;

namespace InsboxService.Data
{
    public class IdeaRepo : IIdeaRepo
    {

        private  UserRepo userreopsitory;
        private readonly AppDbContext _context;

        public IdeaRepo(AppDbContext context)
        {
            _context = context;
        }
        public void CreateIdea(IdeaModel idea)
        {

            if (idea == null)
            {
                throw new ArgumentNullException(nameof(idea));
            }

           

            _context.Ideas.Add(idea);

           
        }

        public IdeaModel DeleteIdea(int ideaId)
        {
            var result = _context.Ideas.FirstOrDefault(e => e.Idnr == ideaId);

            if (result != null)
            {
                _context.Ideas.Remove(result);
                _context.SaveChanges();
                return result;
            }

            return null;
        }

        public IEnumerable<IdeaModel> GetAllIdeas()
        {
            return _context.Ideas.ToList();
        }

        public IdeaModel GetIdeaById(int idnr)
        {
            return _context.Ideas.FirstOrDefault(p => p.Idnr == idnr);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public IdeaModel UpdateIdea(IdeaModel idea)
        {
            if (idea == null)
            {
                throw new ArgumentNullException(nameof(idea));
            }
            _context.Update(idea);
            _context.SaveChanges();
            return _context.Ideas.FirstOrDefault(p => p.Idnr == idea.Idnr);

        }

        void IIdeaRepo.AddEvaluation(Evaluation evaluation, int idnr)
        {
            if (evaluation == null)
            {
                throw new ArgumentNullException(nameof(evaluation));
            }
            IdeaModel idea = _context.Ideas.FirstOrDefault(p => p.Idnr == idnr);
            idea.evaluations.Add(evaluation);
            _context.SaveChanges();

        }

        IEnumerable<Comment> IIdeaRepo.GetAllComments(int idnr)
        {
            return _context.Ideas.FirstOrDefault(p => p.Idnr == idnr).IdeaComments;

        }

        IEnumerable<Evaluation> IIdeaRepo.GetAllEvaluations(int idnr)
        {
            return _context.Ideas.FirstOrDefault(p => p.Idnr == idnr).evaluations;

        }
    }


}