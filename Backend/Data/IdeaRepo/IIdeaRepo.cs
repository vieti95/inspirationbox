using InsboxService.DTO;
using InsboxService.Model;
using Microsoft.AspNetCore.Mvc;

namespace InsboxService.Data
{
    public interface IIdeaRepo
    {
        bool SaveChanges();
        IEnumerable<IdeaModel> GetAllIdeas();
        IdeaModel GetIdeaById(int idnr);
        void CreateIdea(IdeaModel idea);
        public IdeaModel DeleteIdea(int ideaId);
        IdeaModel UpdateIdea(IdeaModel idea);

        void AddEvaluation(Evaluation evaluation, int idnr);

        IEnumerable<Evaluation> GetAllEvaluations(int idnr);
        
        IEnumerable<Comment> GetAllComments(int idnr);
    }
}