using InsboxService.Model;
using Microsoft.EntityFrameworkCore;

namespace InsboxService.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> opt) : base(opt)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usermodel>()
                .HasMany(e => e.MyProjects)
                .WithOne(e => e.Creator)
                .HasForeignKey(e => e.creatoridnr)
                .HasPrincipalKey(e => e.UserIdnr);

            modelBuilder.Entity<Usermodel>()
                .HasMany(e => e.MyIdeas)
                .WithOne(e => e.Creator)
                .HasForeignKey(e => e.useridnr)
                .HasPrincipalKey(e => e.UserIdnr);

            modelBuilder.Entity<Usermodel>()
                .HasMany(e => e.MyEvaluations)
                .WithOne(e => e.User)
                .HasForeignKey(e => e.useridnr)
                .HasPrincipalKey(e => e.UserIdnr);

            modelBuilder.Entity<ProjectModel>()
            .HasOne(e => e.Creator)
            .WithMany(e => e.MyProjects)
            .HasForeignKey(e => e.creatoridnr)
            .HasPrincipalKey(e => e.UserIdnr);
        }
        public DbSet<ProjectModel> Projects { get; set; }
        public DbSet<IdeaModel> Ideas { get; set; }
        public DbSet<Usermodel> Users { get; set; }
    }
}