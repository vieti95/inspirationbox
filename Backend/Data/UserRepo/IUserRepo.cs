using InsboxService.DTO;
using InsboxService.Model;
using Microsoft.AspNetCore.Mvc;

namespace InsboxService.Data
{
    public interface IUserRepo
    {
        bool SaveChanges();
        IEnumerable<Usermodel> GetAllUsers();
        Usermodel GetUserById(int idnr);
        Usermodel CreateUser(UserCreationDTO user);
        IEnumerable<Evaluation> GetAllEvaluations(int idnr);
        IEnumerable<ProjectModel> GetAllProjects(int idnr);
        IEnumerable<IdeaModel> GetAllIdeas(int idnr);
    }
}