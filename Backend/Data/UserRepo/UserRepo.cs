using InsboxService.DTO;
using InsboxService.Model;

namespace InsboxService.Data
{
    public class UserRepo : IUserRepo
    {
        private readonly AppDbContext _context;

        public UserRepo(AppDbContext context)
        {
            _context = context;
        }
        public Usermodel CreateUser(UserCreationDTO user)
        {

            
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            Usermodel usermodel = new Usermodel();
            usermodel.Email = user.Email;
            usermodel.Name = user.Name;
            
            _context.Users.Add(usermodel);

            return usermodel;
        }

        public IEnumerable<Evaluation> GetAllEvaluations(int idnr)
        {
            return _context.Users.FirstOrDefault(p => p.UserIdnr == idnr).MyEvaluations;
        }


        public IEnumerable<ProjectModel> GetAllProjects(int idnr)
        {
            return _context.Users.FirstOrDefault(p => p.UserIdnr == idnr).MyProjects;
        }

        public Usermodel GetUserById(int idnr)
        {
             if (idnr == null)
            {
                throw new ArgumentNullException(nameof(idnr));
            }
            return _context.Users.FirstOrDefault(p => p.UserIdnr == idnr);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public IEnumerable<Usermodel> GetAllUsers()
        {
            return _context.Users.ToList();
        }

        public IEnumerable<IdeaModel> GetAllIdeas(int idnr)
        {
            return _context.Users.FirstOrDefault(p => p.UserIdnr == idnr).MyIdeas;
        }
    }


}
