using System.ComponentModel.DataAnnotations;
using InsboxService.DTO;

namespace InsboxService.Model
{
    public class UserCreationDTO


    {

        
        [Required]
        public string Email { get; set; }

        [Required]
        public string Name{ get; set; }

        
    }
}