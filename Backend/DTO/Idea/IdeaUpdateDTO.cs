using InsboxService.Model;

namespace InsboxService.DTO
{
    public class IdeaUpdateDTO
    {
         
        public string Title { get; set; }
 
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }

        public string Description { get; set; }
       
        
        public String Aim{ get; set; }

        public PotentialUser IdeaPotentialUsers{ get; set; }
        
    }    
}