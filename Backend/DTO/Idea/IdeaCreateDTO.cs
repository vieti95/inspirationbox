using System.ComponentModel.DataAnnotations;
using InsboxService.Model;

namespace InsboxService.DTO
{
    public class IdeaCreateDTO
    {

        //Aim-> as a Description         

        [Required]
        public string Title { get; set; }

        [Required]
        public int useridnr{get; set;}
        public Esector Sector { get; set; }//darf null sein bei potential nicht sector
        
        public Easpect Aspect { get; set; }

        public PotentialUser IdeaPotentialUsers { get; set; }


        public string Aim{ get; set; }
        public string Description { get; set; }

     
    }
}