using InsboxService.Model;

namespace InsboxService.DTO
{
    public class IdeaReadDTO
    {
         
         
        
        public int Idnr { get; set; }

       
        public string Title { get; set; }

     
        //public UserReadDTO Creator { get; set; }
        public int useridnr{get; set;}
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }
        public PotentialUser IdeaPotentialUsers{ get; set; }

        public String Aim{ get; set; }
        public string Description { get; set; }
        //public string/*Benutzer Obejekt*/ ContactPerson { get; set; }

        public List<Comment> IdeaComments{get; set;}
        public List<Evaluation> evaluations{get; set;}
    }    
}