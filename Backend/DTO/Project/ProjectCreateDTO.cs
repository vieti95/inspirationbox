using System.ComponentModel.DataAnnotations;
using InsboxService.Model;

namespace InsboxService.DTO
{
    public class ProjectCreateDTO
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int creatoridnr { get; set; }
        [Required]
        public Esector Sector { get; set; }
        [Required]
        public Easpect Aspect { get; set; }
        public string Description { get; set; }    
        [Required]
        public int contactpersonidnr { get; set; }
    }
}