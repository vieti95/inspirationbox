using InsboxService.Model;

namespace InsboxService.DTO
{
    public class ProjectReadDTO
    {     
        public int Idnr { get; set; }
        public string Title { get; set; }
        //public Usermodel Creator { get; set; }
        public int creatoridnr{get; set;}
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }
        public string Description { get; set; }
        public int/*Benutzer Obejekt*/contactpersonidnr { get; set; }
    }    
}