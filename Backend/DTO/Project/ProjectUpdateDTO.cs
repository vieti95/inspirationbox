using InsboxService.Model;

namespace InsboxService.DTO
{
    public class ProjectUpdateDTO
    {
         
        public string Title { get; set; }
 
        public Esector Sector { get; set; }
        public Easpect Aspect { get; set; }

        public string Description { get; set; }
        public int /*Benutzer Obejekt*/ contactpersonidnr { get; set; }
        
    }    
}