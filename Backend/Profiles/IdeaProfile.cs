using AutoMapper;
using InsboxService.DTO;
using InsboxService.Model;

namespace InsboxService.Profiles
{
    public class IdeaProfile : Profile
    {
        public IdeaProfile()
        {
            CreateMap<IdeaModel, IdeaReadDTO>();
            CreateMap<IdeaCreateDTO, IdeaModel>();
        }
    }
}