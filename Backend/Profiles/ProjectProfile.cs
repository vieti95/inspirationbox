using AutoMapper;
using InsboxService.DTO;
using InsboxService.Model;

namespace InsboxService.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectModel, ProjectReadDTO>();
            CreateMap<ProjectCreateDTO, ProjectModel>();
        }
    }
}