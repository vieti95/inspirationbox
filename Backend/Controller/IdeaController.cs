using AutoMapper;
using InsboxService.Data;
using InsboxService.DTO;
using InsboxService.Model;
using Microsoft.AspNetCore.Mvc;

namespace InsboxService.Controller
{
    [Route("api/[controller]")]//controller=idea <- erster Teil von dem Controller Namen Idea-Controller
    [ApiController]
    public class IdeaController : ControllerBase
    {
        private readonly IIdeaRepo _repository;
        private readonly IMapper _mapper;

        public IdeaController(IIdeaRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<IdeaModel>> GetIdeas()
        {
            Console.WriteLine("get Ideas");
            var IdeaItem = _repository.GetAllIdeas();

            return Ok(_mapper.Map<IEnumerable<IdeaModel>>(IdeaItem));
        }


        [HttpGet("{id}", Name = "GetIdeaById")]
        public ActionResult<IdeaModel> GetIdeaById(int id)
        {
            var IdeaItem = _repository.GetIdeaById(id);

            if (IdeaItem != null)
                return Ok(_mapper.Map<IdeaModel>(IdeaItem));

            return NotFound();


        }

        [HttpPost]
        public ActionResult<IdeaModel> CreateIdea(IdeaCreateDTO ideacreateDTO)
        {
            var ideamodel = _mapper.Map<IdeaModel>(ideacreateDTO);

            _repository.CreateIdea(ideamodel);
_repository.SaveChanges();
            Console.WriteLine("*************");
            
            Console.WriteLine(ideamodel);
            

            var ideareaddto = _mapper.Map<IdeaReadDTO>(ideamodel);

            return CreatedAtRoute(nameof(GetIdeaById), new { Id /*parameter von der Methode*/ = ideareaddto.Idnr }, ideareaddto);

        }

        [HttpPut("{id:int}")]
        public ActionResult<IdeaModel> UpdateIdea(int id, IdeaUpdateDTO updateidea)
        {
            try
            {
                var ideaToUpdate = _repository.GetIdeaById(id);
                updateAttributes(updateidea, ideaToUpdate);

                if (ideaToUpdate == null)
                    return NotFound($"Idea with Id = {id} not found");

                return Ok(_repository.UpdateIdea(ideaToUpdate));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        private static void updateAttributes(IdeaUpdateDTO updateidea, IdeaModel ideaToUpdate)
        {
            ideaToUpdate.Aspect = updateidea.Aspect;
            
            //Cretor stay the same
            ideaToUpdate.Description = updateidea.Description;
            ideaToUpdate.Title = updateidea.Title;
            ideaToUpdate.Sector = updateidea.Sector;
            ideaToUpdate.Aim=updateidea.Aim;
            ideaToUpdate.IdeaPotentialUsers=updateidea.IdeaPotentialUsers;
            //ideaToUpdate.useridnr = updateidea.useridnr;
        }

        [HttpDelete("{id:int}")]
        public ActionResult<IdeaModel> DeleteIdeabyId(int id)
        {
            try
            {
                var ideaToDelete = _repository.GetIdeaById(id);

                if (ideaToDelete == null)
                {
                    return NotFound($"Idea with Id = {id} not found");
                }

                var deletedidea = _repository.DeleteIdea(id);
                var ideareaddto = _mapper.Map<IdeaModel>(deletedidea);
                return ideareaddto;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

    }
}