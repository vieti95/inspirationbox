using AutoMapper;
using InsboxService.Data;
using InsboxService.DTO;
using InsboxService.Model;
using Microsoft.AspNetCore.Mvc;

namespace InsboxService.Controller
{
    [Route("api/[controller]")]//controller=user <- erster Teil von dem Controller Namen User-Controller
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepo _repository;
        private readonly IMapper _mapper;

        public UserController(IUserRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Usermodel>> GetUsers()
        {
            Console.WriteLine("get Users");
            var UserItem = _repository.GetAllUsers();

            return Ok(_mapper.Map<IEnumerable<Usermodel>>(UserItem));
        }


        [HttpGet("{id}", Name = "GetUserById")]
        public ActionResult<Usermodel> GetUserById(int id)
        {
            var UserItem = _repository.GetUserById(id);

            if (UserItem != null)
                return Ok(_mapper.Map<Usermodel>(UserItem));

            return NotFound();

        }
        [HttpGet("projects/{id}", Name = "GetUserProjects")]
        public ActionResult<IEnumerable<ProjectModel>> GetUserProjects(int id)
        {
            var UserItems = _repository.GetAllProjects(id);

            if (UserItems != null)
                return Ok(_mapper.Map<IEnumerable<ProjectModel>>(UserItems));

            return NotFound();

        }

        [HttpPost]
        public ActionResult<Usermodel> CreateUser(UserCreationDTO userCreation)
        {
            //var usermodelx = _mapper.Map<UserCreationDTO>(userCreation);

            Usermodel newuser=_repository.CreateUser(userCreation);
            _repository.SaveChanges();

            var userdtomodel = _mapper.Map<Usermodel>(newuser);

            return CreatedAtRoute(nameof(GetUserById), new { Id /*parameter von der Methode*/ = userdtomodel.UserIdnr }, newuser);

        }

      
    }
}