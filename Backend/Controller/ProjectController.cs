using AutoMapper;
using InsboxService.Data;
using InsboxService.DTO;
using InsboxService.Model;
using Microsoft.AspNetCore.Mvc;

namespace InsboxService.Controller
{
    [Route("api/[controller]")]//controller=project <- erster Teil von dem Controller Namen Project-Controller
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepo _repository;
        private readonly IMapper _mapper;

        public ProjectController(IProjectRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectReadDTO>> GetProjects()
        {
            Console.WriteLine("get Projects");
            var ProjectItem = _repository.GetAllProjects();

            return Ok(_mapper.Map<IEnumerable<ProjectReadDTO>>(ProjectItem));
        }


        [HttpGet("{id}", Name = "GetProjectById")]
        public ActionResult<ProjectReadDTO> GetProjectById(int id)
        {
            var ProjectItem = _repository.GetProjectById(id);

            if (ProjectItem != null)
                return Ok(_mapper.Map<ProjectReadDTO>(ProjectItem));

            return NotFound();

        }

        [HttpPost]
        public ActionResult<ProjectReadDTO> CreateProject(ProjectCreateDTO projectcreateDTO)
        {
            var projectmodel = _mapper.Map<ProjectModel>(projectcreateDTO);

            _repository.CreateProject(projectmodel);
            _repository.SaveChanges();

            var projectreaddto = _mapper.Map<ProjectReadDTO>(projectmodel);

            return CreatedAtRoute(nameof(GetProjectById), new { Id /*parameter von der Methode*/ = projectreaddto.Idnr }, projectreaddto);

        }

        [HttpPut("{id:int}")]
        public ActionResult<ProjectReadDTO> UpdateProject(int id, ProjectUpdateDTO updateproject)
        {
            try
            {
                var projectToUpdate = _repository.GetProjectById(id);
                updateAttributes(updateproject, projectToUpdate);

                if (projectToUpdate == null)
                    return NotFound($"Project with Id = {id} not found");

                return Ok(_repository.UpdateProject(projectToUpdate));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        private static void updateAttributes(ProjectUpdateDTO updateproject, ProjectModel projectToUpdate)
        {
            projectToUpdate.Aspect = updateproject.Aspect;
            projectToUpdate.contactpersonidnr = updateproject.contactpersonidnr;
            //Cretor stay the same
            projectToUpdate.Description = updateproject.Description;
            projectToUpdate.Title = updateproject.Title;
            projectToUpdate.Sector = updateproject.Sector;
        }

        [HttpDelete("{id:int}")]
        public ActionResult<ProjectReadDTO> DeleteProjectbyId(int id)
        {
            try
            {
                var projectToDelete = _repository.GetProjectById(id);

                if (projectToDelete == null)
                {
                    return NotFound($"Project with Id = {id} not found");
                }

                var deletedproject = _repository.DeleteProject(id);
                var projectreaddto = _mapper.Map<ProjectReadDTO>(deletedproject);
                return projectreaddto;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

    }
}