using InsboxService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

 

// ...

 

void ConfigureServices(IServiceCollection services)
{
    services.AddCors(options =>
    {
        options.AddPolicy(name:"AllowMultipleOrigins",
            policy =>
            {
                policy
                    .WithOrigins("https://localhost:53000")
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });
    });

}

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase("InMem"));

builder.Services.AddScoped<IProjectRepo, ProjectRepo>();

builder.Services.AddScoped<IIdeaRepo, IdeaRepo>();

builder.Services.AddScoped<IUserRepo, UserRepo>();

builder.Services.AddControllers();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//Cors
ConfigureServices(builder.Services);
var app = builder.Build();
//**
app.UseCors("AllowMultipleOrigins");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

PrepDb.PrepPop(app);

app.Run();
